# REJS PO MAZURACH - dodatkowy sprzet dla załogi
## Sprzęt biwakowy i naprawczy
 - [ ] saperka
 - [ ] sierkieka 
 - [ ] śrubokręt 
 - [ ] klucze
 - [ ] kombinerki 
 - [ ] klucz do szekli. 
 - [ ] ewentualnie multitool zawierający powyższe ( koniecznie ze stali 154 cm )
 - [ ] apteczka
 - [ ] moskitiera

## Nawigacja i sztuka żelowania
 - [ ] mapy
 - [ ] podręcznik żeglarstwa
 - [ ] patent żeglarski ( przynajmniej jeden by się przydał, żeby nam jachty wydali )

## Gotowanie
 - [ ] deska do krojenia 
 - [ ] ostre noże 
 - [ ] duży garnek 
 - [ ] duża patelnia
 - [ ] mała patelnia
 - [ ] tarka do grana padano
 - [ ] durszlak
 - [ ] frenchpress/kawiarka/dripper
 - [ ] młynek do kawy
 - [ ] korkociąg

## Elektronika:
 - [ ] przedłużacze
 - [ ] falownik ( przetwornica dc/ac )

## Podstawowe zakupy:
 - [ ] worki na śmieci
 - [ ] gąbki
 - [ ] płyn do mycia naczyń
 - [ ] zapałki, zapalcznika
 - [ ] sól, pieprz, przyprawy
 - [ ] olej/oliwa
 - [ ] woda
 - [ ] papier toaletowy
 - [ ] ręczniki papierowe 
 - [ ] mokre chusteczki 
 - [ ] papierowe talerzyki 

## Dodatkowe 
 - [ ] grill + węgiel 
 - [ ] shaker 
 - [ ] głośnik bluetooth
 - [ ] namiot
 - [ ] Tent do jachtu
