# REJS PO MAZURACH - checklista załogi
 
## Co spakować
> Idea jest prosta - zwykłe ubrania + coś nieprzemakalnego i ciepłego. Jeżeli ktoś ma profesjonalne sztormiaki, szeklownik, namierniki, obuwie żeglarskie to ok, ale nie ma sensu tego kupować. Jak ktoś złapie zajawkę i będzie chciał pływać po morzu to wtedy można się bawić w inwestowanie w jakieś profesjonalne/drogie sprzęty. Do zwykłego turystycznego pływania wystarczy w większości zwykły ewipunek turystyczny. 

 - [ ] Torba sportowa / worek żeglarski / miękki plecak. Torba powinna być miękka. Nie zabieramy popularnych twardych walizek z kółkami uwielbianych w podróżach samolotem, ponieważ na jachcie mogą po prostu nie wejść do bakisty.

 - [ ] Ubranie na złą pogodę - Sztormiak / kurtka nieprzemakalna / odzież BHP / peleryna. - Idealnie aby odzież miała przyzwoitą odporność na deszcz i wiatr. Ja osobiście preferuję lekkie wodoodporne kurtki + softshell/polar z windstoperem pod spód. Na pierwszy rejs weźcie po prostu co macie nieprzemakalnego bez udziwnień.

 - [ ] Buty z miękką jasną, podeszwą, która nie porysuje pokładu, z dobrym bieżnikiem zapewniającym przyczepność ( trampki są ok ). Staramy się unikać podeszwy z ciemnej gumy bo lubi zostawiać wielkie rysy/smugi na laminatowym pokładzie, a nie chcemy tego szorować. 

 - [ ] Czapka z daszkiem lub coś co chroni czerep przed słońcem. Podczas płynięcia przy ładnej pogodzie będziemy cały czas wystawieni na słońce, a o udar nie trudno.

 - [ ] Rękawice do szorowania lin. Tutaj zostawiam dowolność czy to będą robocze rękawice gumowe, rowerowe czy techniczne. Rękawice mają być takie że jak przyłożycie do nich bardzo tępy nóż to wam ręki nie potnie. Podczas pracy szotami (liny od żagli) można się nabawić oparzeń. Rękawice rowerowe spełniają swoją rolę. 
  
 - [ ] spodnie lub spodenki do śmigania po jachcie, codziennej egzystencji, gotowania, spania, 
 - [ ] podkoszulki
 - [ ] sweter / polar / golf 
 - [ ] buty - crocsy, klapki basenowe, trampki, adidasy - oczywiście nie rysujące pokładu
 - [ ] kąpielówki / strój kąpielowy
 - [ ] ręcznik (minimum 2, idealnie aby były szybkoschnące)
 - [ ] pasek do spodni
 - [ ] komin lub chusta

## Inne:
 - [ ] śpiwór + poduszka - to obowiązkowo - na jachcie nie ma pościeli
 - [ ] prześcieradło - to dodatkowo - jak ktoś lubi mieć czyste prześcieradło pod śpiwór.
 - [ ] okulary przeciwsłoneczne
 - [ ] krem UV (zaczynamy od 50 schodzimy do 30/20)
 - [ ] buty do wody lub pod prysznic
 - [ ] karta multisport ( czasem da się zaoszczędzić na prysznicu w porcie )

## Sprzęt i gadżety 
> Należy pamiętać, że na jachcie podczas płynięcia mamy dostęp do prądu 12V (gniazda zapalniczki, czasem USB). 230V jest dostępne jedynie podczas postoju w porcie. 

 - [ ] Ładowarka do elektroniki osobistej (gniazdo zapalniczki na 12V + 230V + kable)
 - [ ] Powerbanki
 - [ ] Latarka - najlepiej czołówka.
 - [ ] Smycz na telefon - zabezpieczenie przed wpadnięciem telefonu do wody. 

## Kosmetyka i lekarstwa
> Jachty posiadają na wyposażeniu apteczkę, ale znajdują się w niej głównie plastry i bandaże. Dobrze mieć ze sobą:

 - [ ] Własne lekarstwa
 - [ ] Mydło/Żel/Szampon
 - [ ] Szczoteczka do zębów
 - [ ] Pasta do zębów
 - [ ] Chusteczki
 - [ ] Chusteczki mokre
 - [ ] Maść na ukąszenia, jeżeli uprawiamy szuwarowo bagienne pływanie na mazurach
 - [ ] Pół kilo kawy speciality - najlepiej coś powyżej 85 punktów - Na rejs może być zmielona ( jakoś to przeżyjemy ) - https://www.coffeea7.pl/blog/co-to-jest-kawa-speciality-i-punktacja/
